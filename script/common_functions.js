function find_parent_by_class(parent_class, start)
{
    parent = start.parent();
	while (!parent.hasClass(parent_class) && !parent.is('body'))
	{
		var temp = parent.parent();
		parent = temp;
	}
	
	if (parent.hasClass(parent_class))
	{
		return parent;
	}
	
	return false;
}