// operate create, edit and delete requests

$(document).ready(function(){
    $(".btn_new").click(new_record);
    $(".btn_edit").click(edit_record);
    $(".btn_delete").click(delete_record);
});

function new_record()
{
    //request new record editor based on current table input value
    var a = $(this);
    a.unbind("click");
    var current_table = $("#current_table").val();
    $.ajax({
        url: "/admin/create_record/"+current_table,
        success:function(data){
            var editor = $("#editor_container");
            editor.append(data);
            editor.show();
            begin_edit(editor);
            a.click(new_record);
        }  
    });
}
function edit_record()
{
    //request edit form
    var a = $(this);
    a.unbind("click");
    
    var current_table = $("#current_table").val();
    var current_id = $(this).parent().find(".record_id").val();
    
    $.ajax({
        url: "/admin/edit_record/"+current_table+"/"+current_id,
        success:function(data){
            var editor = $("#editor_container");
            editor.append(data);
            editor.show();
            begin_edit(editor);
            a.click(new_record);
        }  
    });
}
function delete_record()
{
    var record = find_parent_by_class("record_container", $(this));
    var current_table = $("#current_table").val();
    var current_id = record.find(".record_id:eq(0)").val();
    
    $.ajax({
        url: "/admin/delete_record/"+current_table+"/"+current_id,
        success:function(data){
            record.remove();
            check_record_order();
        }
    });
}

function begin_edit(editor)
{
    //unbind all editing function before starting a new editing session
    editor.find(".normal_button").unbind("click");
    
    //if some input change its value, the "Close button switch to "Cancel"
    editor.find("input").change(function(){
        $("#changed_state").val(1);
        $(".btn_cancel").val("Cancel");
    });
    
    editor.find("#btn_save").click(save_record);
    editor.find("#btn_cancel").click(cancel_edit)
}
function save_record()
{
    //Save the record but do not close the dialog
    //use for both edit and create new
    //there is no ID for new record?
    
    var current_table = $("#current_table").val();
    var current_id = $("#current_id").val();  // potential problem
    
    if (typeof current_id === "undefined")
    {
        current_id = "new";
    }
    
    $.ajax({
        url: "/admin/save_record/"+current_table+"/"+current_id,
        success:function(data){
            $("#changed_state").val(0);
            $(".btn_cancel").val("Close");
    
        }
    });
    
}
function cancel_edit()
{
    //check if record is saved
    var editor = $("#editor_container");
    var changed = $("#changed_state").val();
    if (changed !== 0)
    {
        //make an alert before closing
    }
    //clear and hide editor
    
    editor.empty();
    editor.hide();
    //update list in manager if needed
    
    
}
function check_record_order()
{
    //update order of remaining records if necessary
    //This function cycle through all ".record_container" and attempt to assign new index number
}