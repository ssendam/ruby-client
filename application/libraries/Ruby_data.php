<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Ruby_data {
	public function request($data)
	{
		$data['api_author'] = API_AUTHOR;
		$data['api_signature'] = API_SIGNATURE;
		$data = http_build_query($data);
		$server_url = RUBY_API_ENDPOINT;
		$json_data = $this->send_request($server_url, $data);
		return json_decode($json_data);
	}
	
	protected function send_request($url, $data)
	{
		$refer = base_url();
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL,$url); 
	    curl_setopt ($ch, CURLOPT_POST, 1); 
	    curl_setopt ($ch, CURLOPT_POSTFIELDS, $data); 
	    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt ($ch, CURLOPT_REFERER, $refer);
	    $result= curl_exec($ch);
	    curl_close($ch);
	    return $result;
	}
}
