<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Author extends CI_Controller {
    public function index()
    {
        //	 call can be ajax or normal http requests
        //   check current editing comic
        $this->check_login();
    	$query_data = $this->input->get();
    	
    	if (!empty($query_data))
    	{
    		if ($query_data['type'] == "ajax")	
    		{
    			
    		}
    		else
    		{
    			switch ($query_data['action'])
    			{
    				
    			}
    			
    			$this->load->view();
    		}
    	}
    	else
    	{
    		$this->check_comic();
    	}
    	
    }
    
    private function check_comic()
    {
    	$comic_data = $this->session->userdata("comic_id");
    	if (empty($comic_data))
    	{
    		$query_string = "type=normal&action=query&table=comic&field[]=id&field[]=name&field[]=plot&field[]=img";
    		
    	}
    	else
    	{
    		$query_string = "action=view&table=comic&field=all";
    	}
    	redirect("/author?$query_string", "refresh");
    }
    private function get_data($query_data)
    {
        $query_string = $this->input->get();
        echo $query_string['table']; // => "comic"
        foreach ($query_string['field'] as $field)
        {
        	echo $field;
        }
    }
    
    private function check_login()
    {
        $user_data = $this->session->userdata('logged');
        
        if ($user_data)
        {
            return true;
        }
        else
        {
            redirect("/author/login", "refresh");
        }
    }
    
    
    // public functions
    
    public function login()
    {
        $this->load->view("author/login");
    }
    public function logout()
    {
    	$this->session->sess_destroy();
    	redirect('/author', 'refresh');
    }
    public function authenticate()
    {
    	$userdata['logged'] = TRUE;
    	$this->session->set_userdata($userdata);
    	redirect('/author', 'refresh');
    	/*
        $data = $this->input->post();
        
        $this->load->database();
        
        $this->load->model('author_security');
        
        $result = $this->author_security->authenticate($data);
        
        $this->db->close();
        
        if ($result !== false)
        {
            $userdata = array('id' => $result['id'],
					'username' => $result[$data['username']],
					'type' => 'author',
					'logged' => TRUE);
		    $this->session->set_userdata($userdata);
        }
        else
        {
        
        }
    	*/   
    }
    
}