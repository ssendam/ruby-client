<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Abc extends CI_Controller
{

	public function index()
	{
	    $this->load->library('form_validation');
	    $this->load->library('sampl');
	    $this->load->model('sample');
	    $this->sample->index();
		$this->load->view('abc/Abc');

	}
	public function Adc()
	{
        $this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'username', 'callback_username_check');
		$this->form_validation->set_rules('password', 'password', 'required');
	    $this->form_validation->set_rules('username', 'username', 'trim|numeric');
		if ($this->form_validation->run() == FALSE)
		{
		$this->load->view('abc/Abc');
		}
	    $inputFormUser['inputFormUser'] = $this->input->post();
	    $inputFormUser['data'] =	$this->Sample();
		$this->load->view('abc/Adc',$inputFormUser);
	
	}
	private function Sample()
	{
	    $data = array(
           array(
              'title' => 'My title' ,
              'name' => 'My Name' ,
              'date' => 'My date'
           ),
           array(
              'title' => 'Another title' ,
              'name' => 'Another Name' ,
              'date' => 'Another date'
           )
        );  
         return $data;
	}
	public function username_check($str)
	{
		if ($str === 'pitpull')
		{
			$this->form_validation->set_message('username_check', 'The %s field can not be the word "pitpull"');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}